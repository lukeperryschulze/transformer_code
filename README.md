# Transformer Implementation in PyTorch

This repository contains a PyTorch implementation of the Transformer model as described in the paper "Attention Is All You Need" by Vaswani et al.

## Overview

The Transformer model is a neural network architecture that has achieved state-of-the-art results in various natural language processing tasks. This implementation provides a foundation for understanding and experimenting with the Transformer architecture.

##TODO
- Note that this project is a work in progress. 