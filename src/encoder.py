import torch
import math
from torch import nn
import torch.nn.functional as F

# Note: The following code was heavily inspired by CodeEmporium: https://www.youtube.com/watch?v=g3sEsBGkLU0

# Init

d_model = 512  # input dimension for our embeddings
num_heads = 8  # heads for multihead attention
drop_prob = 0.1  # probability for dropout weights
batch_size = 30
max_sequence_length = 200  # largest number of tokens we can pass in [Note: If we are below, than we automatically add <PAD> tokens
ffn_hidden = 2048  # size of the feedforward hidden layer
num_layers = 5  # How many encoder layers we want to have

# Note that we only calculate the dot product here, because we adhere to the original paper
# In current work, we usually linearly transform our values with additional matrices
def scaled_dot_product(q, k, v, mask = None):

    # Save the dimension of the attention head
    d_k = q.size()[-1]  # In PyTorch, you can use negative indices to count dimensions from the end. So, q.size()[-1] gets the size of the last dimension of the tensor q.
    # Note the transpose -1, -2 indices, this is because we do not want to transpose over the batch dimension
    scaled = torch.matmul(q, k.transpose(-1, -2) /math.sqrt(d_k))
    if mask is not None:
        scaled += mask  # Broadcasting add. So the first 'batch' dimension does not need to match
    attention = F.softmax(scaled, dim=-1)
    values = torch.matmul(attention, v)
    return values, attention


class MultiHeadAttention(nn.Module):
    def __init__(self, d_model, num_heads):
        super().__init__()
        self.d_model = d_model
        self.num_heads = num_heads
        self.head_dim = d_model // num_heads  # // automatically rounds down the answer
        self.qkv_layer = nn.Linear(d_model, 3*d_model)
        self.linear_layer = nn.Linear(d_model, d_model)

    # Input form: [batch size, max_sequence_length, embedding_dim]
    def forward(self, input, mask = None):
        batch_size, sequence_length, d_model = input.size()
        print(f"input.size(): {input.size()}")
        qkv = self.qkv_layer(input)
        print(f"qkv.size(): {qkv.size()}")
        # Here we break down the qkv vectors into our different (attention) heads
        qkv = qkv.reshape(batch_size, sequence_length, self.num_heads, 3*self.head_dim)
        # change ordering of our data (Now: (batch, heads, sequence length, 3*self.head_dim))
        qkv = qkv.permute(0, 2, 1, 3)
        # Split-off key, query and value 'values' (i.e. 3*self.head_dim becomes head_dim for each of the tensors)
        q, k, v = qkv.chunk(3, dim=-1)
        # calculate attention and corresponding values
        values, _ = scaled_dot_product(q, k, v)
        # stack up the attention vectors
        values = values.reshape(batch_size, sequence_length, self.num_heads * self.head_dim)
        out = self.linear_layer(values)
        return out



class EncoderLayer(nn.Module):
    def __init__(self, d_model, ffn_hidden, num_heads, drop_prob):
        super(EncoderLayer, self).__init__()
        self.attention = MultiHeadAttention(d_model = d_model, num_heads = num_heads)
        self.norm1 = LayerNormalization(parameters_shape=[d_model])
        self.dropout1 = nn.Dropout(p = drop_prob)
        self.ffn = PositionwiseFeedForward(d_model = d_model, hidden = ffn_hidden, drop_prob = drop_prob)
        self.norm2 = LayerNormalization(parameters_shape = [d_model])
        self.dropout2 = nn.Dropout(p = drop_prob)

    def forward(self, x):
        residual_input = x.clone()
        x = self.attention(x, mask = None)
        # Dropouts always after the layer, since we want to set out activations to zero (droput neurons)
        x = self.dropout1(x)
        x = self.norm1(x + residual_input)
        residual_input = x
        x = self.ffn(x)
        x = self.dropout2(x)
        x = self.norm2(x + residual_input)





class Encoder(nn.Module):
    def __init__(self, d_model, ffn_hidden, num_heads, drop_prob, num_layers = 3):
        super().__init__()
        self.layers = nn.Sequential(*[EncoderLayer(d_model, ffn_hidden, num_heads, drop_prob)
                                      for _ in range(num_layers)])

    def forward(self, input):
        x = self.layers(input)
        return x